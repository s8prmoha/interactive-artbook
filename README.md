# Interactive Art Book

The main idea of this project is to create an art book that can be interacted with an AR app on a smart phone. Often any painting or art work is open to interpretation to the viewer with its still imagery. But adding augmented reality can add another dimension to it and make the artist express themselves better.
The images considered in this project will be some basic sketches like a picture of an open book, lamp or just a page with some borders. When any smartphone installed with the app will hover over the art it will show a different image or a video or animation that will explain the idea of the art. The art itself does not have any meaning.
To start with the interaction the user is provided with the art book, consisting of the chosen images and pictures and the app installed on their smartphone. As the app opens it will directly access the phone’s camera to look for the art pictures. As the camera detects the images, it will change or move depending on the feature that has been overlaid on it. For interaction the following techniques are used:
1. Overlaying with Images.
    The image target is overlaid with 2D single or multiple images depicting another form of the image or giving the idea that the image is moving. So, when the camera detects the image it displays the overlaid images in a sequence.
2. Overlaying with Videos.
    The target images is overlaid with videos. Thus adding more features to the basic sketch. The video plays as soon as the camera detects it. It continues to play on loop till the camera is moved away. Once it finds the image it starts playing again.
3. Overlaying with 3D objects.
    The target images here is overlaid with 3D objects. When the camera pans over to the target image, the object appears on it. This 3D object can be rotated, enlarged and reduced in size with the provided buttons on the app.

## Prerequisites

1. Unity Editor: This project is currently implemented on unity 2019.3. While installing click the check boxes “Vuforia Augmented Reality Support” and “Android Build support” in      the ‘Choose Components’ window.
2. Vuforia SDK: Create an account in Vuforia and get the license key. This license key is added to the app that will be created. For more details use the reference [14]
3. Targets: Create target database in vuforia. You can create your own targets and add to it. For the targets to work properly with the app, keep as many features as possible.        More deatails on how to create targets is explained in the section creating target.
4. Adobe Photoshop CS2: Photoshop is required to edit the images that you will be working on. This will come handy while importing images without backgrounds.
5. Video editor: Any video editor can be used. You will require these to create .mp4 files with images or edit the videos.

## Settings to keep in mind

- Select "Android" platform when creating the app
- Import vuforia unity package and add vufroia license key to the vuforia configuration in unity.
- check the checkbox "Load Object Targets" in the inspector window of AR Camera.
- Import the target database.

## Creating Targets

As this project involves making an art book, we need to create the targets ourselves. Although to have an idea on how to go ahead with the implementation in the starting we worked with images obtained online. The targets should have the following properties
- Have as many trackable features as possible (a grade of 4 and above star in vuforia). Targets can preferably have a defined border so that augmented layer can be placed properly. 
- Also make sure that the features on the target are well distributed.
- They should be of maximum size 2mb and in .png or .jpeg format. In the case of formats, jpeg is preferable as sometimes the .png formats are not supported and cause an error on upload.

The following steps explain how to create target images in Vuforia
~~~
1. Create an account in Vuforia and open Target Manager. 
2. Click on Add Database and give it a name. Make sure you select "Device" type.
3. Click on the created database and go to "Add Target". Choose the type of target, upload the image,give it a size and name the image target. Preferably use .jpeg image otherwise .png gives error when it is not 24bit.
4. Once the target is added, the image would be processed and a rating would be given to it. Make sure it has a good rating so that the tracking is efficient.
5. Click on "Download Database" and import the unity package in the Unity Editor.
~~~

## Adding Animations

In order to add more action to the 3D models, animations were added. Animations make the 3D models feel more alive. To begin with, some basic animations were tried on. Here we are using InnerDriveStudios’ Skullchest to work on the animations. The animationtest folder contains all files related to this section.

The animator panel can be accessed from the Window menu option. Create an animation with the name “chestopen”. Enable the recording mode and choose the duration of the animation. In this the case the duration was selected to be 30 seconds. The top of the chest was animated under the transform section as rotation. 

In case of multiple animations, the flow of the animations can be changed by using the animator. The animator is accessed by clicking the icon created next to your animation in the asset folder. In this case it is named as top. In the animator, the default animation is changed and the loop condition on the animation is removed. 

## Adding UI Buttons

On adding the animations, they simply run when the camera pans on the target. There is no control on the animations. In order to add the control, UI buttons are added. So, when the buttons are clicked the linked animations work.
To implement this, a UI canvas was added to the scene and it screen size was set to Screen size 1280x720.
To this canvas a panel and then buttons were added (all under UI). Each button was placed on bottom of the canvas with names “open” and “close”. These UI buttons were made to control the opening and closing of the skullchest. 
To link these buttons to the animation, first a script is written. A new script is added to the top section of the skullchest from ‘Add Component’. The edited script is available in 'testanimation' folder as 'chestbehav'.
Once the code is saved, in the inspector panel for ‘open’ button under ‘On click’ add an event. To that event link part that is animated (here the top of the chest) and choose the function to open the chest. Do the same steps for closing the chest.

## Importing Images without background

To import images without background, Adobe Photoshop actions can be used. The action called ‘AlphaUtility.atn’ can be downloaded from here. To add this to the action panel in Photoshop, click an arrow shaped icon on the top right corner of the action panel. A list of sub options will appear, click on load options and navigate to the folder where your action is saved and select it. After importing, now an option name ‘AlphaUtility’ appears in the action panel.
The following steps give directions to separate an image from its background.

~~~
- Separate the part of the image you want to use from the rest. This can be done by selecting it with a ‘magnetic lasso’ tool or ‘quick selection’ tool, whatever suits best.
- Then, right click the selection and choose ‘layer via cut’. A new layer gets created with the selection. Delete the layer with rest of the image.
- Select the layer with the image, and go to ‘Layer’ in the menu bar and choose ‘matting’ and ‘defringe’.
- Create a duplicate layer of the one with the image. And select it.
- In the actions panel choose ‘Dilute’ under AlphaUtility and perform it. It will create another duplicate layer of it with thick outline. Merge all the created layers.
- Select the layer with original image and the image outline. Switch to channels panel and click ‘Save selection as channel’ at the bottom of the panel. This will create a new alpha channel. Save the file as .psd file. 
- In unity, import the file into the assets folder and add the image to a plane.
- On adding the image, a material folder appears. Open it and click on the material with the image. In the inspector panel under shader option choose ‘unlit’ and then ‘Transparent Cutout’.
~~~

And you have the image without background!


The apk to the final version of ArtBook can be found [here](https://oc.cs.uni-saarland.de/owncloud/index.php/s/feY8SLBZTpgPBSE).

