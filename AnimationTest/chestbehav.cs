﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chestbehav : MonoBehaviour
{
    private Animator Anim;
    // Start is called before the first frame update
    void Start()
    {
        Anim = GetComponent<Animator>();
        Anim.speed = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Animopen()
    {
        Anim.Play("openchest", -1, 0f);
        Anim.speed = 1f;
    }

    public void Animclose()
    {
        Anim.Play("closechest", -1, 0f);
        Anim.speed = 1f;
    }
}
